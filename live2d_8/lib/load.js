var 引流 = [
]

let mod11   = "http://localhost:8000/";
let mod1 = "https://cdn.jsdelivr.net/gh/fishpigbird/live2d_1999_js/";


function getRandomModelList(mod1,index) {
  const model = [
    mod1 + "live2d_8/305302_yaxian/305302_yaxian.model3.json",
    //mod1 + "live2d_8/Ava/Ava.model3.json",
    mod1 + "live2d_8/304101_madierda/304101_madierda.model3.json",
    mod1 + "live2d_8/304102_madierda/304102_madierda.model3.json",
    mod1 + "live2d_8/304103_madierda/304103_madierda.model3.json",
    mod1 + "live2d_8/303802_wuseyue/303802_wuseyue.model3.json",
    mod1 + "live2d_8/305602_jiexika/305602_jiexika.model3.json",
    mod1 + "live2d_8/v1a5_305303_yaxian/v1a5_305303_yaxian.model3.json",
  ];

  if(index == 1){
  // 使用 Fisher-Yates 算法进行随机排序
  for (let i = model.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [model[i], model[j]] = [model[j], model[i]];
  }
}
  return model;
}



// 使用方法
//
const randomModelList = getRandomModelList(mod1,1);



const initConfig = {
  mode: "fixed",
  hidden: true,
  content: {
    link: 引流[Math.floor(Math.random() * 引流.length)],
    welcome: ["Hi!"],
    touch: "",
    skin: ["诶，想看看其他神秘学家吗？", "Hi ，Time is reversed"],
    custom: [
      { "selector": ".comment-form", "text": "Content Tooltip" },
      { "selector": ".home-social a:last-child", "text": "Blog Tooltip" },
      { "selector": ".list .postname", "type": "read" },
      { "selector": ".post-content a, .page-content a, .post a", "type": "link" }
    ],
  },
  night: "toggleNightMode()",
  model: randomModelList,
  tips: true,
  onModelLoad: onModelLoad
}

function Load_Live2d() {
  pio_reference = new Paul_Pio(initConfig)

  pio_alignment = "left"

  // Then apply style
  pio_refresh_style()
}


  function isMobile() {
    var ua = window.navigator.userAgent.toLowerCase();
    ua = ua.indexOf("mobile") || ua.indexOf("android") || ua.indexOf("ios");
    //return window.innerWidth < 500 || ua !== -1;
    return  ua !== -1;
}


function onModelLoad(model) {
  const container = document.getElementById("pio-container")
  const canvas = document.getElementById("pio")
  const modelNmae = model.internalModel.settings.name
  const coreModel = model.internalModel.coreModel
  const motionManager = model.internalModel.motionManager

  let touchList = [
    {
      textEn: "动作1",
      motion: "b_idle"
    }
  ]

  let expList = [
    {
      textEn: "表情1",
      expression: "e_deyi"
    },
    {
      textEn: "表情2",
      expression: "e_pofang"
    }
  ]



  canvas.onclick  = canvas.ontouchstart = function () {
    if (motionManager.state.currentGroup !== "Idle") return

    console.log("clicktest")


    if (isMobile) {
      // 在移动设备上执行的逻辑
      var actionButton = document.querySelector('.pio-action');
      actionButton.style.opacity = actionButton.style.opacity === '1' ? '0' : '1';
    }


    const action = pio_reference.modules.rand(touchList)
    playAction(action)

    const expression = pio_reference.modules.rand(expList)
    playExpression(expression)

  }

  var audio = new Audio();

  function playAction(action) {

    if (audio) {
      audio.pause();
      }   
    audio = new Audio(action.sound);
    audio.volume = 0.7;
    var audioTime;
    audio.addEventListener('loadedmetadata', function() {
        // 在loadedmetadata事件中获取duration
        audioTime = audio.duration * 1000;
        console.log('音频时长：', audioTime + ' 毫秒');

        action.textEn && pio_reference.modules.renderEn(action.textEn,audioTime)
        //显示时间为语言时间。
        action.textZh && pio_reference.modules.renderZh(action.textZh,audioTime)

    });
    
    audio.play(); 



    action.motion && pio_reference.model.motion(action.motion)

    //pio_reference.model.expression('e_pofang');

    if (action.from && action.to) {
      Object.keys(action.from).forEach(id => {
        const hidePartIndex = coreModel._partIds.indexOf(id)
        TweenLite.to(coreModel._partOpacities, 0.6, { [hidePartIndex]: action.from[id] });
        // coreModel._partOpacities[hidePartIndex] = action.from[id]
      })

      motionManager.once("motionFinish", (data) => {
        Object.keys(action.to).forEach(id => {
          const hidePartIndex = coreModel._partIds.indexOf(id)
          TweenLite.to(coreModel._partOpacities, 0.6, { [hidePartIndex]: action.to[id] });
          // coreModel._partOpacities[hidePartIndex] = action.to[id]
        })
      })
    }
  }


  function playExpression(expression) {
    //expression.textEn && pio_reference.modules.render(expression.textEn)
    expression.expression && pio_reference.model.expression(expression.expression)
  }



  

if (modelNmae === "304101_madierda" || modelNmae === "304102_madierda" || modelNmae ==="304103_madierda" ){
  container.dataset.model = "304101_madierda"
  //initConfig.content.skin[1] = ["~"]
  //playAction({ motion: "Idle" })

  function getRandomMotion() {
    const motions = [
        "b_idle2",
        "b_yaotou",
        "b_diantou",
        "b_baoxiong",
        "b_zhi",
        "b_zhanbu1",
        "b_zhanbu2",
        "b_zhanshi1",
        "b_zhanshi2",
        "b_zhanshi3",
        "b_idle",
        "b_chaofeng",
        "b_toutou2",
        "b_toutou3",
        "b_toutou1",
        "b_zhanbu3"
    ];
  

    // 使用 Fisher-Yates 算法进行随机排序
    for (let i = motions.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [motions[i], motions[j]] = [motions[j], motions[i]];
    }
  
    return motions[0];
  }

  touchList1 = [
    {
      textName:"",
      textEn: "The nature of divination is deduction based on reality ... It is not always correct, but I surely am.",
      textZh: "哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈.",
      motion: "b_toutou",
      sound: "live2d_8/304101_madierda/sounds/toutou_nature.wav"
    }
  ]
  //动作文件脚本修正
  //英文中文声音文件脚本输入
  //动作默认随机getRandomMotion()，特殊的固定来改。

  touchList = [ 
    {
      textName:"初遇",
      textZh:"喂，你为什么会在这里？！……等等，这是不是说明十四行诗也……",
      textEn:"Hey, why are you here? ... Wait, does it mean Sonetto is also ...",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_01.mp3"
    }, 
    {
      textName:"箱中气候",
      textZh:"美妙的太阳，就像玛蒂尔达一样耀眼的太阳！怎么？你有意见吗？",
      textEn:"The glorious sun, as bright as Matilda! Huh, any problem?",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_02.mp3"
    }, 
    {
      textName:"致未来",
      textZh:"伟大的玛蒂尔达同学将成为基金会历史上最好的外派调查员，和她的助手十四一起，出色地完成了所有的任务。",
      textEn:"The Great Matilda will become the Foundation's best on-site investigator ever. Together with her assistant Sone ... they will excel at all missions assigned.",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_03.mp3"
    }, 
    {
      textName:"孑立",
      textZh:"玛蒂尔达现在很有空，你有什么问题都可以来问她了……咳咳！听到了吗？",
      textEn:"Matilda is now available. Feel free to ask her anything ... Ahem! Did you hear me?",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_04.mp3"
    }, 
    {
      textName:"问候",
      textZh:"进门之前，请先敲门！退出去再来一次，等我说“请进”之后再进来！",
      textEn:"Knock first! Now walk out and wait till I say Come in please!",
      motion: "b_zhi",
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_05.mp3"
    }, 
    {
      textName:"朝晨",
      textZh:"你们对食物缺乏灵感，比方说，法式面包只有配上蒜香黄油，才能发挥出美妙的香气。看看我是怎么做的，来！",
      textEn:"You have no taste in food. Say, baguette tastes fabulous only with Beurre maitre d'hotel. Watch me and learn, come on!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_06.mp3"
    }, 
    {
      textName:"信任-朝晨",
      textZh:"今天这条丝巾很衬你的衬衫，选得好。请继续保持这样高品位的生活方式，时刻谨记你可是在与协助教员玛蒂尔达同行！",
      textEn:"The silk scarf kinda fits your shirt. Nice. Please maintain the great taste for lifestyle and always keep in mind that you are working with the monitor assistant Matilda!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_07.mp3"
    }, 
    {
      textName:"夜暮",
      textZh:"熏香蜡烛、百里香、鼠尾草……很好，布置完成了。水晶球啊，请给予我答案……",
      textEn:"Bougies parfumées, thym, sauge ... Super. Décoration terminée. Oh boule de crystal, montre-moi l'avenir, s'il te plaît ...",
      motion:"b_zhanbu1",
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_08.mp3"
    }, 
    {
      textName:"信任-夜暮",
      textZh:"玛蒂尔达当然是第一名，哼哼，就连睡觉，也是第一个钻进床铺的！",
      textEn:"Matilda is of course the best. Huh. She's even the first one who gets into bed!",
      motion:"b_baoxiong",
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_09.mp3"
    }, 
    {
      textName:"帽檐与发鬓",
      textZh:"听好了，假如你有什么不懂的问题，都可以来问了不起的玛蒂尔达。不管是神秘学还是外面的世界，我都无所不知，无所不晓！",
      textEn:"Listen, if you have anything unclear, come to the Great Matilda. No matter arcanum or things in the outside world, Matilda is omniscient.",
      motion:"b_chaofeng",
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_10.mp3"
    }, 
    {
      textName:"袖与手",
      textZh:"又光滑，又柔软，在梦里……她的手就像棉花糖……看，看我做什么！你什么都没听到！",
      textEn:"Si douce, si délicate comme un doux rêve ... ses mains sont comme de la barbe à papa ... What ... what are you looking at! You didn't hear anything!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_11.mp3"
    }, 
    {
      textName:"衣着与身形",
      textZh:"你不要太得意！我现在也是协助教员了！早晚——早晚十四行诗会过来当我的助手……",
      textEn:"Don't get carried away! I am the monitor assistant! Someday ... someday Sonetto will become my assistant ...",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_12.mp3"
    }, 
    {
      textName:"嗜好",
      textZh:"十四行诗？你怎么突然提起那个讨厌的家伙了？咳咳，听好了，第一名只能是我，她迟早会被我打败。",
      textEn:"Sonetto? Why do you suddenly mention that annoying gal? Ahem, listen, the best student could only be me! I will definitely best her!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_13.mp3"
    }, 
    {
      textName:"赞赏",
      textZh:"伟大的教员玛蒂尔达，给予了你高度的赞扬……而你，大可以对此感到自豪。",
      textEn:"The Great Monitor Matilda highly recognizes you. And you ... can be really proud of that.",
      motion:"b_baoxiong",
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_14.mp3"
    }, 
    {
      textName:"亲昵",
      textZh:"哼，第一名的玛蒂尔达，给了垫底的你与她一起出去玩的荣誉……尽管高兴吧。",
      textEn:"Huh, the best student Matilda kindly offers you, the worst student, the honor to hang out with her ... Enjoy.",
      motion:"b_baoxiong",
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_15.mp3"
    }, 
    {
      textName:"闲谈Ⅰ",
      textZh:"你究竟有什么不一样的地方呢？十四行诗为什么甘心当你的助手？明明在学校的时候你是我的“下级”才对！",
      textEn:"Why are you so very special? Why is Sonetto willing to be your assistant? You were my subordinate when we were at school!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_16.mp3"
    }, 
    {
      textName:"闲谈Ⅱ",
      textZh:"占卜的本质是基于现行一切的推演……它并非永远正确，只不过，我总是对的。",
      textEn:"The nature of divination is deduction based on reality ... It is not always correct, but I surely am.",
      motion:"b_toutou",
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_17.mp3"
    }, 
    {
      textName:"独白",
      textZh:"预言是一个危险的能力，妈妈拉着我的手说我必须完全掌握它，才能不为自己带来危险，我不明白她的意思，但好在我总是第一名。直到，直到……",
      textEn:"It's dangerous to be a foreteller. My mum used to hold my hands and tell me that I'll be free from danger only if I've taken complete command of foreseeing. I didn't understand her but luckily I was always the best student. Jusqu'à, jusqu'à ...",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_mianvoc_hero3041_18.mp3"
    }, 
    {
      textName:"入队",
      textZh:"只有我吗？……她……在吗？",
      textEn:"Anyone else? ... Is ... she ... here?",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_19.mp3"
    }, 
    {
      textName:"战前",
      textZh:"让他们领教一下玛蒂尔达·布翁尼小姐的能力！",
      textEn:"Time to present the marvelous Matilda Bouanich!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_20.mp3"
    }, 
    {
      textName:"择选咒语Ⅰ",
      textZh:"我知道怎么做。",
      textEn:"I know how to do it.",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_21.mp3"
    }, 
    {
      textName:"择选咒语Ⅱ",
      textZh:"别命令我。",
      textEn:"Don't tell me what to do.",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_22.mp3"
    }, 
    {
      textName:"择选高阶咒语",
      textZh:"最棒的，最好的，玛蒂尔达！",
      textEn:"The best, the greatest, Matilda!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_23.mp3"
    }, 
    {
      textName:"择选至终的仪式",
      textZh:"我能看见，我能够很清楚地看见……",
      textEn:"I can see, I am able to see clearly ...",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_24.mp3"
    }, 
    {
      textName:"释放神秘术Ⅰ",
      textZh:"哼！",
      textEn:"Heh!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_25.mp3"
    }, 
    {
      textName:"释放神秘术Ⅰ",
      textZh:"喂，看好了！",
      textEn:"Hey, witness me!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_26.mp3"
    }, 
    {
      textName:"释放神秘术Ⅱ",
      textZh:"当然！",
      textEn:"Of course!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_27.mp3"
    }, 
    {
      textName:"释放神秘术Ⅱ",
      textZh:"该尝尝苦头了！",
      textEn:"This is your gloomy fate!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_28.mp3"
    }, 
    {
      textName:"召唤至终的仪式",
      textZh:"你的命运已然注定！",
      textEn:"Your fate is doomed!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_29.mp3"
    }, 
    {
      textName:"受敌Ⅰ",
      textZh:"见鬼！",
      textEn:"Zut!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_30.mp3"
    }, 
    {
      textName:"受敌Ⅱ",
      textZh:"可恶！",
      textEn:"Merde!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_31.mp3"
    }, 
    {
      textName:"战斗胜利",
      textZh:"法兰西万岁，玛蒂尔达万岁，哈哈～",
      textEn:"Vive la France, Vive la Matilda! Ha ha!",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_32.mp3"
    }, 
    {
      textName:"洞悉",
      textZh:"哼，还差得远呢……",
      textEn:"Huh, far from enough ...",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_33.mp3"
    }, 
    {
      textName:"洞悉之底",
      textZh:"天才的玛蒂尔达，于今天让世界臣服在她的脚下……以第一名的成绩。",
      textEn:"Aujourd'hui, Matilda la surdouée domine le monde entier ... en tant que meilleure élève.",
      motion:getRandomMotion(),
      sound: mod1 + "live2d_8/304101_madierda/sounds/En_play_hero3041fightingvoc_34.mp3"
    }]


  expList = [
    {
      textEn:  "表情1", 
      expression:  "e_nanguo"
    }, 
    {
      textEn:  "表情2", 
      expression:  "e_kaixin"
    }, 
    {
      textEn:  "表情3", 
      expression:  "e_deyi"
    }, 
    {
      textEn:  "表情4", 
      expression:  "e_shengqi"
    }, 
    {
      textEn:  "表情5", 
      expression:  "e_pofang"
    }, 
    {
      textEn:  "表情6", 
      expression:  "e_chijing"
    }, 
    {
      textEn:  "表情7", 
      expression:  "e_idle"
    }, 
    {
      textEn:  "表情8", 
      expression:  "e_niupi"
    }
  ]
  } else if (modelNmae === "303802_wuseyue"){
    container.dataset.model = "303802_wuseyue"
    //initConfig.content.skin[1] = ["~"]
    //playAction({ motion: "Idle" })

  
    function getRandomMotion() {
      const motions = [
        "b_diantou",
        "b_shenzhan",
        "b_liaofa",
        "b_tiaopi",
        "b_fumo",
        "b_yaotou",
        "b_taishou",
        "b_tuopan",
        "b_idle"
      ];
    

      // 使用 Fisher-Yates 算法进行随机排序
      for (let i = motions.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [motions[i], motions[j]] = [motions[j], motions[i]];
      }
    
      return motions[0];
    }
    
    touchList = [ 
      {
        textName:"初遇",
        textZh:"初次见面，请多指教。司辰……小姐，对吗？",
        textEn:"はじめまして。どうぞよろしくお願いします。 Ms. ... Timekeeper, is it?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_1.mp3"
      }, 
      {
        textName:"箱中气候",
        textZh:"“隐约雷鸣 阴霾天空 但盼风雨来 能留你在此”“隐约雷鸣 阴霾天空 即使天无雨 我亦留此地”……这是店里的客人教奴家的。",
        textEn:"「雷神の少し響みてさし曇り雨もふらぬか君を留めむ」 「雷神の少し響みてふらずとも吾は留らむ妹し留めば」 The customers in the cafe taught me this.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_2.mp3"
      }, 
      {
        textName:"致未来",
        textZh:"奴家想和老板娘还有孩子们在一起……当下如此，未来亦是。",
        textEn:"I would like to be with the boss and children ... Now, and in the future.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_3.mp3"
      }, 
      {
        textName:"孑立",
        textZh:"有谁在吗？……好安静……咖啡店里的大家现在怎么样了呢？",
        textEn:"どなたかいらっしゃいませんか？ ... It's so quiet ... How is everyone in the cafe now?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_4.mp3"
      }, 
      {
        textName:"问候",
        textZh:"两名绅士走入了气派的料理屋……啊，司辰小姐！十分抱歉，奴家沉迷于小说中，没有注意到您来了……",
        textEn:"Two gentlemen walked into a fine brick building ... Oh, Ms. Timekeeper! My sincerest apologies. I was immersed in a novel and did not notice you ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_5.mp3"
      }, 
      {
        textName:"朝晨",
        textZh:"早上好，请问您今天想要咖啡还是茶呢？",
        textEn:"Good morning. Would you like some tea or coffee today?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_6.mp3"
      }, 
      {
        textName:"信任-朝晨",
        textZh:"早上好，早饭已经准备好了，有味噌汤、米饭……您没有早上吃米饭的习惯吗？",
        textEn:"Good morning, breakfast is ready. There is miso soup, rice ... Are you not used to having rice for breakfast?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_7.mp3"
      }, 
      {
        textName:"夜暮",
        textZh:"这么晚了，您还要继续工作吗？",
        textEn:"It's late. Are you still working?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_8.mp3"
      }, 
      {
        textName:"信任-夜暮",
        textZh:"以前在牢房里，一到了晚上，看守的大哥都会跟奴家讲讲日间他遇到的趣闻……不知道他现在过得怎么样了。",
        textEn:"When I was in jail, the guard in the jail would tell me about the fun parts of his day once night fell ... I wonder how he is doing now.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_9.mp3"
      }, 
      {
        textName:"帽檐与发鬓",
        textZh:"过往奴家从来不曾注重过自身容貌。全靠老板娘的教导，奴家才有幸略知一二。",
        textEn:"私、お洒落はからっきし…　でしたが、 女将さんのご指導で少しだけ馴染むようになりました。",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_10.mp3"
      }, 
      {
        textName:"袖与手",
        textZh:"过去迫于生计，奴家做了某些无可奈何的事而招致了这副……饰品。",
        textEn:"For my survival, I did some thing that resulted in me getting this ... accessory.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_11.mp3"
      }, 
      {
        textName:"衣着与身形",
        textZh:"请不要这样……啊，您没有恶意？",
        textEn:"Please don't ... Ah, you didn't mean to?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_12.mp3"
      }, 
      {
        textName:"嗜好",
        textZh:"兴趣？奴家并没有想过这种事情，因为奴家一直以来都只能考虑眼前发生的事……",
        textEn:"Hobby? I have never thought about it, because the only thing need to be worried about is the immediate future ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_13.mp3"
      }, 
      {
        textName:"赞赏",
        textZh:"好厉害……奴家想应该没有什么事是您做不到的吧？只要穿上女仆装，您也一定能很好地胜任这份工作……",
        textEn:"すごいです ... There must be nothing you cannot do. If you wear a maid outfit, you will be great at this job too ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_14.mp3"
      }, 
      {
        textName:"亲昵",
        textZh:"东京府的孩子，都会穿好看的衣服吗……啊、奴家只是随口一提，请别放在心上……",
        textEn:"Do children in Tokyo ... wear nice clothing? Oh, I just brought it up. Please do not mind me.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_15.mp3"
      }, 
      {
        textName:"闲谈Ⅰ",
        textZh:"不、不……奴家不会再偷东西了……即、即使一不小心拿了，也会立刻还回去的……嗯……",
        textEn:"N- No, I won't steal anything anymore ... E-even if I take something by accident, I will return it right away ... Yes ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_16.mp3"
      }, 
      {
        textName:"闲谈Ⅱ",
        textZh:"“雁儿啊，勿再悲啼。从今开始，我身也漂泊。”偶尔一次与咖啡店的客人们谈及奴家的过往，他们教给了奴家这支俳句。",
        textEn:"「なくな雁　今日から我も　旅人ぞ」 ... I spoke about the past with customers at the cafe once. This Haiku was taught by them.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_17.mp3"
      }, 
      {
        textName:"独白",
        textZh:"安全的居所，干净的衣服，美味的食物，安稳的工作……这一切，简直就像是梦境一样，真害怕……会有醒来的那天。",
        textEn:"A safe place, clean clothes, delicious food, a stable job ... All these are like a dream. Thinking of waking up from this is ... terrifying.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_mianvoc_hero3038_18.mp3"
      }, 
      {
        textName:"入队",
        textZh:"团伙作案……嗯，不。没什么。",
        textEn:"Group crime ... Oh, no. It's nothing.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_19.mp3"
      }, 
      {
        textName:"战前",
        textZh:"那么，奴家出发了。",
        textEn:"では、いってきます。",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_20.mp3"
      }, 
      {
        textName:"择选咒语Ⅰ",
        textZh:"请交给奴家。",
        textEn:"私にお任せください。",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_21.mp3"
      }, 
      {
        textName:"择选咒语Ⅱ",
        textZh:"奴家会照吩咐做的。",
        textEn:"I will do as asked.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_22.mp3"
      }, 
      {
        textName:"择选高阶咒语",
        textZh:"奴家不会手下留情。",
        textEn:"手加減は致しませんよ。",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_23.mp3"
      }, 
      {
        textName:"择选至终的仪式",
        textZh:"又到了一个好时节。",
        textEn:"What a great season.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_24.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"一只小兔子，两只小兔子。",
        textEn:"One rabbit, two rabbits.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_25.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"去吧——！",
        textEn:"Gooo!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_26.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"不好意思……",
        textEn:"My apologies ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_27.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"适当地利用磨难。",
        textEn:"Take advantage of adversity.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_28.mp3"
      }, 
      {
        textName:"召唤至终的仪式",
        textZh:"赏樱的时节到了。",
        textEn:"Time to admire the cherry blossoms.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_29.mp3"
      }, 
      {
        textName:"受敌Ⅰ",
        textZh:"……好痛！",
        textEn:"……いたい！",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_30.mp3"
      }, 
      {
        textName:"受敌Ⅱ",
        textZh:"不行……！",
        textEn:"No ...!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_31.mp3"
      }, 
      {
        textName:"战斗胜利",
        textZh:"结束了吗？真是太好了，我们一起回去吧。",
        textEn:"Is it over? That's great. Let's go back together.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_32.mp3"
      }, 
      {
        textName:"洞悉",
        textZh:"承、承蒙您的厚爱……",
        textEn:"Th-Thank you so much for your care ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_33.mp3"
      }, 
      {
        textName:"洞悉之底",
        textZh:"好像有什么发生了变化，但……又好像什么都没有变。",
        textEn:"Something seems to have changed, but ... not really.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/303802_wuseyue/sounds/En_play_hero3038_34.mp3"
      }]


expList = [
  {
    expression:  "e_chijing", 
    textEn:  "表情1"
  }, 
  {
    expression:  "e_idle", 
    textEn:  "表情2"
  }, 
  {
    expression:  "e_haixiu", 
    textEn:  "表情3"
  }, 
  {
    expression:  "e_kaixin", 
    textEn:  "表情4"
  }, 
  {
    expression:  "e_haoqi", 
    textEn:  "表情5"
  }, 
  {
    expression:  "e_nanguo", 
    textEn:  "表情6"
  }, 
  {
    expression:  "e_shengqi", 
    textEn:  "表情7"
  }
]


    
  } else if (modelNmae === "305602_jiexika"){
    container.dataset.model = "305602_jiexika"
    //initConfig.content.skin[1] = ["~"]
    //playAction({ motion: "Idle" })

    function getRandomMotion() {
      const motions = [
        "b_diantou",
        "b_moxiaba",
        "b_waitou",
        "b_yaotou",
        "b_shenshou",
        "b_motoufa",
        "b_idle",
        "b_bianshen"
      ];
    

      // 使用 Fisher-Yates 算法进行随机排序
      for (let i = motions.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [motions[i], motions[j]] = [motions[j], motions[i]];
      }
    
      return motions[0];
    }

    touchList = [ 
      {
        textName:"初遇",
        textZh:"你好，好朋友，现在我们能一直在一起了。我在这段时间内想了许多我们能一起做的事儿，首先是……在草地上打滚！",
        textEn:"Hello, dear friend. From now on, we can be together all the time. I thought of many things we can do. Firstly ... let's roll in the grass!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_01.mp3"
      }, 
      {
        textName:"箱中气候",
        textZh:"当太阳这样暖和的时候，我会去湖里洗澡，然后躺在岸边打盹儿，直到太阳把我烘干～",
        textEn:"In the days when the sun warmly slants on me, I would bathe in the lake and enjoy a nap on the shore, until my fur was nicely dried under the sun ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_02.mp3"
      }, 
      {
        textName:"致未来",
        textZh:"更远、更大、更神奇的地方——我会看到星星，看到舞蹈的蛇，看到你们所说的一切，看到我所想见到的一切，美好的一切。",
        textEn:"There is a place, far away from here, and it's grander, more wonderful than all places⁠— place where I can see the stars twinkling in the sky and snakes dancing in the grass, as well as other things you've talked about and things I've always wanted to see. These beautiful things.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_03.mp3"
      }, 
      {
        textName:"孑立",
        textZh:"九十七，九十八，九十九……一百！我要出来抓你了哦——",
        textEn:"97, 98, 99 ... 100! I'm coming for you ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_04.mp3"
      }, 
      {
        textName:"问候",
        textZh:"很高兴再见到你，你不在的这段时间，我把你的毛发都收集到了那个抽屉里……",
        textEn:"I'm so glad we meet again. When you were gone, I collected every hair of yours and put them in that drawer ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_05.mp3"
      }, 
      {
        textName:"朝晨",
        textZh:"太阳冲破雾气，从树枝和山脉里浮起来，我想它尝起来像是生鸡蛋的味道……！",
        textEn:"In the morning, the sun would pierce through the fog, float on the tips of the trees and the mountains ... I bet it tastes like a raw egg ...!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_06.mp3"
      }, 
      {
        textName:"信任-朝晨",
        textZh:"你的被窝好像比草垛还柔软，我能……钻进去试试吗？",
        textEn:"Your bed feels even softer than my haystack. Can I ... crawl in and join you?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_07.mp3"
      }, 
      {
        textName:"夜暮",
        textZh:"我的朋友到了，想见见他们吗？嘘，瞧，就在窗帘后面，看见他的脚了吗～？",
        textEn:"My friends are here. Do you want to meet them? Shh ... Look! Over there behind the curtain. Can you see his feet?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_08.mp3"
      }, 
      {
        textName:"信任-夜暮",
        textZh:"你喜欢恶魔？还是幽灵？还是喜欢提着电锯的男人？我必须知道你喜欢什么样的怪物，这样我才能更好地藏在你床底，给你一个惊喜！",
        textEn:"Do you prefer demons or ghosts? Or the man with a chainsaw? I have to figure out what your favorite monster is, so that I can better surprise you under your bed!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_09.mp3"
      }, 
      {
        textName:"帽檐与发鬓",
        textZh:"当我吃了“药”的时候，脑子里总能听到一种轻飘飘的笛声……但是笛声？嗯？什么是笛声？",
        textEn:"Every time I took that medicine, I could hear a soft, light whistling melody... Whistling? Hmm? What is whistling?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_10.mp3"
      }, 
      {
        textName:"袖与手",
        textZh:"我的手和你的手拉在一起，以后你到哪儿去都能带着我啦！",
        textEn:"My hands in your hands, friendship never ends! You can take me to any places you go!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_11.mp3"
      }, 
      {
        textName:"衣着与身形",
        textZh:"这是“环”——结实的、漂亮的环！它们紧紧地抱着我，让我和衣裳还有床铺牢固地贴在一起～我想……我们之间也需要这样的环。",
        textEn:"This is my band ... Look how nice and pretty it is! It embraces me, securing me to my clothes and the bed ... I think, between you and me, we can also use a band like this.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_12.mp3"
      }, 
      {
        textName:"嗜好",
        textZh:"梦里……在奇怪的梦里，那片草原总在等着我，我听见轻快的笛声……",
        textEn:"In that dream ... that strange dream, the grassland is waiting for me, and I can hear the joyful whistle ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_13.mp3"
      }, 
      {
        textName:"赞赏",
        textZh:"举起左边的蹄子！再举起右边的蹄子！做得很好！就像是我们接受的训练那样——高高地跳起来，哇哇地嘶吼吧！",
        textEn:"Raise the left hoof! Then the right hoof! Well done! Now, just like what we have done in the training⁠—jump up, and roooar!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_14.mp3"
      }, 
      {
        textName:"亲昵",
        textZh:"当我是“安妮”的时候，我会尽可能温柔地说话，像是这样——把语速变得慢慢的，要忍住不能发笑，也不能用蹄子踢我讨厌的人……那不容易，但我做到了！你应该像是那些穿着白衣服的人一样夸夸我！",
        textEn:"When I was Anne, I spoke as gently as possible, like this ... Slow down, hold in the chuckles, and resist the urge to give those annoying people a good kick ... That was difficult, but I did it! You should say something nice to me, like those people in white do!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_15.mp3"
      }, 
      {
        textName:"闲谈Ⅰ",
        textZh:"我看到他们把小狗拴了起来，你也会对我这样吗？没关系，我不在乎这个。只要我们在一起，谁把谁拴住都无所谓……",
        textEn:"I saw them put a leash on that puppy. Are you going to do the same thing to me? It's okay. I will be fine with it. As long as we are together, it doesn't matter who is on which end of the leash ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_16.mp3"
      }, 
      {
        textName:"闲谈Ⅱ",
        textZh:"现在你身上有股特别的味道，它——它……它像是蜂蜜！像是鸢尾花，也像是被碾碎的瓢虫，我喜欢这个！",
        textEn:"There's a special scent on you. It ... it ... smells like honey! Or Iris, or a crushed ladybug. I like it!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_17.mp3"
      }, 
      {
        textName:"独白",
        textZh:"为什么朋友们总是有“更重要的事”要做呢？让我们一起把那些多余的“镜头”删掉吧，只留下永远的、甜美的、大家一起笑着的画面。",
        textEn:"Why do my friends always have more important things to do? We should delete the unwanted shots in our lives, only keeping the takes which recorded the sweet, laughing moments.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_mianvoc_hero3056_18.mp3"
      }, 
      {
        textName:"入队",
        textZh:"森林派对～",
        textEn:"A forest party!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_19.mp3"
      }, 
      {
        textName:"战前",
        textZh:"嗨，你们好！",
        textEn:"Hey, hello there!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_20.mp3"
      }, 
      {
        textName:"择选咒语Ⅰ",
        textZh:"认真，集中！",
        textEn:"Be serious. Stay focused!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_21.mp3"
      }, 
      {
        textName:"择选咒语Ⅱ",
        textZh:"跑起来！",
        textEn:"Move your legs!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_22.mp3"
      }, 
      {
        textName:"择选高阶咒语",
        textZh:"一起来玩吧～",
        textEn:"Come play with us!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_23.mp3"
      }, 
      {
        textName:"择选至终的仪式",
        textZh:"就像是训练时一样！",
        textEn:"Just like in the training!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_24.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"翻花绳～",
        textEn:"Cat's cradle!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_25.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"丝绒闪电～！",
        textEn:"Velvet lightning!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_26.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"上啊！上啊！",
        textEn:"Go! Go!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_27.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"说“你好”！",
        textEn:"Say hello!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_28.mp3"
      }, 
      {
        textName:"召唤至终的仪式",
        textZh:"哦！别担心！我把一切都准备好了——一切都准备好了。",
        textEn:"Oh, don't worry! I have prepared it all. I ... have prepared it all.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_29.mp3"
      }, 
      {
        textName:"受敌Ⅰ",
        textZh:"呀！",
        textEn:"Aah!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_30.mp3"
      }, 
      {
        textName:"受敌Ⅱ",
        textZh:"呜……",
        textEn:"*sobs*",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_31.mp3"
      }, 
      {
        textName:"战斗胜利",
        textZh:"快起来，还有下一轮呢！",
        textEn:"Come on! Stand up! There's one more round!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_32.mp3"
      }, 
      {
        textName:"洞悉",
        textZh:"呀，谢谢你为我梳毛。好舒服呀，真希望你也能试试看！",
        textEn:"Oh, thank you for grooming me. How nice ... I wish you could try it someday!",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_33.mp3"
      }, 
      {
        textName:"洞悉之底",
        textZh:"我已经准备好了，准备好了告诉所有人我的名字，准备好了带来尖叫和眼泪……他们会像我喜欢他们那样喜欢我吗？",
        textEn:"I'm ready⁠—ready to tell everyone my name, ready to make them scream and cry ... Will they like me in the same way as I like them?",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305602_jiexika/sounds/En_play_hero3056fightingvoc_34.mp3"
      }]


expList = [
  {
    expression:  "e_kaixin", 
    textEn:  "表情1"
  }, 
  {
    expression:  "e_chijing", 
    textEn:  "表情2"
  }, 
  {
    expression:  "e_idle", 
    textEn:  "表情3"
  }, 
  {
    expression:  "e_shengqi", 
    textEn:  "表情4"
  }, 
  {
    expression:  "e_nanguo", 
    textEn:  "表情5"
  }, 
  {
    expression:  "e_biyan", 
    textEn:  "表情6"
  }, 
  {
    expression:  "e_haixiu", 
    textEn:  "表情7"
  }
]
  } else if (modelNmae === "305302_yaxian"){
    container.dataset.model = "305302_yaxian"
    //initConfig.content.skin[1] = ["~"]
    //playAction({ motion: "Idle" })


    function getRandomMotion() {
      const motions = [
        "b_yaxian",
        "b_diantou",
        "b_huangpingzi",
        "b_idle",
        "b_fumo",
        "b_yaotou",
        "b_changge"   
      ];
    

      // 使用 Fisher-Yates 算法进行随机排序
      for (let i = motions.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [motions[i], motions[j]] = [motions[j], motions[i]];
      }
    
      return motions[0];
    }

    touchList = [ 
      {
        textName:"初遇",
        textZh:"你好，我想我们并不陌生。……我负责照看人们的牙齿，在每一个熟睡的夜晚。",
        textEn:"Hello, I think we have been acquainted. ... I am responsible for your teeth, every night you fall asleep.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_01.mp3"
      }, 
      {
        textName:"箱中气候",
        textZh:"泥土变得松软，适宜种植，帮助干瘪的躯体重获新生。",
        textEn:"The soil has become crumbly, aerated, suitable for plantation, and conducive for the dry body to regenerate.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_02.mp3"
      }, 
      {
        textName:"致未来",
        textZh:"走向未来预示着成长，预示着牙齿的脱落……与新生。",
        textEn:"Heading towards the future means growing up, means teeth falling, and ... new life.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_03.mp3"
      }, 
      {
        textName:"孑立",
        textZh:"这儿的每一个人都紧紧咬着牙，以手掌掩嘴。今天不是复仇的日子。",
        textEn:"Everyone here gnashes their teeth and covers their mouths with their hands. Humph! Come on, today is not a revenge day.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_04.mp3"
      }, 
      {
        textName:"问候",
        textZh:"无需赘言，例行检查——请先张开你的嘴巴。",
        textEn:"Stop talking. It's time for a checkup ... Please open your mouth.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_05.mp3"
      }, 
      {
        textName:"朝晨",
        textZh:"用牙膏或洁牙粉打磨牙齿会让它们更光亮，也更健康。薄荷将为你的牙齿带来恒久的勇气，让它们不感畏惧，不从牙床上逃离。",
        textEn:"Toothpaste or tooth powder will make your teeth glowing and healthy. Mint makes your teeth courageous, fearless, and never escape from the gum.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_06.mp3"
      }, 
      {
        textName:"信任-朝晨",
        textZh:"昨晚收获颇丰，这将是一个美味的早晨。",
        textEn:"Harvest last night brings a big breakfast delight.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_07.mp3"
      }, 
      {
        textName:"夜暮",
        textZh:"成长期的孩子们夜中时常躁动不安，他们活泼好动，情绪多变……面对睡前故事，便像是吃不饱的怪物，然后，我会为他们唱歌。",
        textEn:"Kids in early childhood are hyperactive at night. They are energetic and have mood swings ... Their demand for bedtime stories is a bottomless pit. So I always prepare songs for them.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_08.mp3"
      }, 
      {
        textName:"信任-夜暮",
        textZh:"你安然入睡便可。玛丽有只小绵羊，小绵羊，小绵羊……",
        textEn:"Please! Have a sweet dream. Mary had a little lamb, little lamb, little lamb ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_09.mp3"
      }, 
      {
        textName:"帽檐与发鬓",
        textZh:"我看见牙仙们上下翻飞，它们须发扑朔，金丝口袋里装着孩子们纤弱的乳牙。",
        textEn:"I have seen those toothfairies flying in the air with their hair dancing freely. Inside their gold-string pockets lie the fragile baby teeth collected from the kids.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_10.mp3"
      }, 
      {
        textName:"袖与手",
        textZh:"动作要又轻又快、稳定而准确，掐住脖子、翅膀或是背脊，留意避开腹部或脚踝。像是这样……",
        textEn:"You need to act swiftly and gently, firmly and precisely, to grab hold of their neck, wings, or the back. Don't hurt their abdomen and ankles. Like this ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_11.mp3"
      }, 
      {
        textName:"衣着与身形",
        textZh:"不必拘泥于颜色，黑大褂能让每一颗致病的牙齿无处躲藏。",
        textEn:"The color is flexible. My black overcoat helps expose every single pathogenic tooth.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_12.mp3"
      }, 
      {
        textName:"嗜好",
        textZh:"绿森蚺、狼、鲨鱼……动物们的牙齿令我大开眼界，我各有珍藏；但归根结底，唯有孩童小巧的乳牙能让我心神荡漾。它们柔软、细腻，不可弥补。",
        textEn:"Green anaconda, wolf, shark ... Animal teeth really surprise me. I have collected a lot, but only the baby teeth of kids would amaze me. Those teeth are soft, delicate, and irreplaceable.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_13.mp3"
      }, 
      {
        textName:"赞赏",
        textZh:"做得很好。我会奖励你一颗太妃糖。",
        textEn:"Well done. Here is a toffee for you.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_14.mp3"
      }, 
      {
        textName:"亲昵",
        textZh:"这是你的乳牙，整整二十颗，都在这里。",
        textEn:"Your baby teeth. Twenty in total. They're all here.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_15.mp3"
      }, 
      {
        textName:"闲谈Ⅰ",
        textZh:"我与我的家庭在人类与神秘学家之间行走，我们获得钱财、地位与优渥的生活，但与此同时也失去朋友、归所与社会的认可。",
        textEn:"My family and I walked among humans and arcanists. We gain possessions, status, and decent life. But at the same time, we lose friends, belongingness, and recognition by society.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_16.mp3"
      }, 
      {
        textName:"闲谈Ⅱ",
        textZh:"我记得你，维尔汀小姐。当你还在学校时，当我还在学校时，你总是校医室的常客。一半头疼脑热是装的，另一半擦伤和摔伤是真的。我猜想你是为了安慰糖果来的，因为当你拿到太妃糖罐时，总是笑着的。",
        textEn:"I remember you, Ms. Vertin. When you were still a student, when I was the dentist there, you always visited the clinic. All those headaches you had were faked, but the wounds and bruises were real. I think your frequent visits were for the comforting candies, weren't they? Because you always smiled when you received the toffees.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_17.mp3"
      }, 
      {
        textName:"独白",
        textZh:"当你在人类之间，便以人类的方式生活。当你在神秘学家之中，又当以神秘学家的方式生活。“保持自我”总是滋养傲慢之心，紧接着带来毁灭。",
        textEn:"When in human society, do as the humans do; when in the arcanist community, do as the arcanists do. Too attached to your identity always causes arrogance and consequentially brings destruction.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_18.mp3"
      }, 
      {
        textName:"入队",
        textZh:"我为一颗有恙的牙而来。",
        textEn:"I'm here for a worn tooth.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_19.mp3"
      }, 
      {
        textName:"战前",
        textZh:"开始“口镜”观察。",
        textEn:"Dental checkup.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_20.mp3"
      }, 
      {
        textName:"择选咒语Ⅰ",
        textZh:"别紧张。",
        textEn:"Relax.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_21.mp3"
      }, 
      {
        textName:"择选咒语Ⅱ",
        textZh:"不必担心。",
        textEn:"No worries.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_22.mp3"
      }, 
      {
        textName:"择选高阶咒语",
        textZh:"一切都会好起来的。",
        textEn:"It will all be fine.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_23.mp3"
      }, 
      {
        textName:"择选至终的仪式",
        textZh:"我们都需要片刻安宁。",
        textEn:"We need some peaceful moments.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_24.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"请不要抵抗。",
        textEn:"Please don't resist.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_25.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"感谢配合。",
        textEn:"Thank you for your cooperation.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_26.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"呼吸。",
        textEn:"Breathe.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_27.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"我能闻到薄荷油的香气。",
        textEn:"I smell the scent of mint oil.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_28.mp3"
      }, 
      {
        textName:"召唤至终的仪式",
        textZh:"阿嚏，阿嚏，我们都跌倒。",
        textEn:"A-tishoo, a-tishoo. We all fall down.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_29.mp3"
      }, 
      {
        textName:"受敌Ⅰ",
        textZh:"嗯——",
        textEn:"Hmmm ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_30.mp3"
      }, 
      {
        textName:"受敌Ⅱ",
        textZh:"停止。",
        textEn:"Stop.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_31.mp3"
      }, 
      {
        textName:"战斗胜利",
        textZh:"它们比基金会的孩子们要顽皮得多。",
        textEn:"They are much naughtier than the Foundation's kids.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_32.mp3"
      }, 
      {
        textName:"洞悉",
        textZh:"衷心感谢，我离我的愿景又近了一步。",
        textEn:"I owe you a debt of gratitude. I am one more step closer to my dream.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_33.mp3"
      }, 
      {
        textName:"洞悉之底",
        textZh:"睡吧，安心地睡吧，这是个甜蜜的夜晚……",
        textEn:"Sleep, sleep in peace. This is a sweet, sweet night ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_34.mp3"
      }]

    

expList = [
  {
    expression:  "e_idle", 
    textEn:  "表情1"
  }, 
  {
    expression:  "e_haixiu", 
    textEn:  "表情2"
  }, 
  {
    expression:  "e_jingya", 
    textEn:  "表情3"
  }, 
  {
    expression:  "e_nanguo", 
    textEn:  "表情4"
  }, 
  {
    expression:  "e_weixiao", 
    textEn:  "表情5"
  }, 
  {
    expression:  "e_shengqi", 
    textEn:  "表情6"
  }
]
  } else if (modelNmae === "v1a5_305303_yaxian"){
    container.dataset.model = "v1a5_305303_yaxian"
    //initConfig.content.skin[1] = ["~"]
    //playAction({ motion: "Idle" })

    function getRandomMotion() {
      const motions = [
        "b_bawan",
        "b_huipai",
        "b_yaotou",
        "b_liaofa",
        "b_idle",
        "b_diantou",
        "b_fumo",
        "b_changge"
      ];
    

      // 使用 Fisher-Yates 算法进行随机排序
      for (let i = motions.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [motions[i], motions[j]] = [motions[j], motions[i]];
      }
    
      return motions[0];
    }


    touchList = [ 
      {
        textName:"初遇",
        textZh:"你好，我想我们并不陌生。……我负责照看人们的牙齿，在每一个熟睡的夜晚。",
        textEn:"Hello, I think we have been acquainted. ... I am responsible for your teeth, every night you fall asleep.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_01.mp3"
      }, 
      {
        textName:"箱中气候",
        textZh:"泥土变得松软，适宜种植，帮助干瘪的躯体重获新生。",
        textEn:"The soil has become crumbly, aerated, suitable for plantation, and conducive for the dry body to regenerate.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_02.mp3"
      }, 
      {
        textName:"致未来",
        textZh:"走向未来预示着成长，预示着牙齿的脱落……与新生。",
        textEn:"Heading towards the future means growing up, means teeth falling, and ... new life.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_03.mp3"
      }, 
      {
        textName:"孑立",
        textZh:"这儿的每一个人都紧紧咬着牙，以手掌掩嘴。今天不是复仇的日子。",
        textEn:"Everyone here gnashes their teeth and covers their mouths with their hands. Humph! Come on, today is not a revenge day.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_04.mp3"
      }, 
      {
        textName:"问候",
        textZh:"无需赘言，例行检查——请先张开你的嘴巴。",
        textEn:"Stop talking. It's time for a checkup ... Please open your mouth.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_05.mp3"
      }, 
      {
        textName:"朝晨",
        textZh:"用牙膏或洁牙粉打磨牙齿会让它们更光亮，也更健康。薄荷将为你的牙齿带来恒久的勇气，让它们不感畏惧，不从牙床上逃离。",
        textEn:"Toothpaste or tooth powder will make your teeth glowing and healthy. Mint makes your teeth courageous, fearless, and never escape from the gum.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_06.mp3"
      }, 
      {
        textName:"信任-朝晨",
        textZh:"昨晚收获颇丰，这将是一个美味的早晨。",
        textEn:"Harvest last night brings a big breakfast delight.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_07.mp3"
      }, 
      {
        textName:"夜暮",
        textZh:"成长期的孩子们夜中时常躁动不安，他们活泼好动，情绪多变……面对睡前故事，便像是吃不饱的怪物，然后，我会为他们唱歌。",
        textEn:"Kids in early childhood are hyperactive at night. They are energetic and have mood swings ... Their demand for bedtime stories is a bottomless pit. So I always prepare songs for them.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_08.mp3"
      }, 
      {
        textName:"信任-夜暮",
        textZh:"你安然入睡便可。玛丽有只小绵羊，小绵羊，小绵羊……",
        textEn:"Please! Have a sweet dream. Mary had a little lamb, little lamb, little lamb ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_09.mp3"
      }, 
      {
        textName:"帽檐与发鬓",
        textZh:"我看见牙仙们上下翻飞，它们须发扑朔，金丝口袋里装着孩子们纤弱的乳牙。",
        textEn:"I have seen those toothfairies flying in the air with their hair dancing freely. Inside their gold-string pockets lie the fragile baby teeth collected from the kids.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_10.mp3"
      }, 
      {
        textName:"袖与手",
        textZh:"动作要又轻又快、稳定而准确，掐住脖子、翅膀或是背脊，留意避开腹部或脚踝。像是这样……",
        textEn:"You need to act swiftly and gently, firmly and precisely, to grab hold of their neck, wings, or the back. Don't hurt their abdomen and ankles. Like this ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_11.mp3"
      }, 
      {
        textName:"衣着与身形",
        textZh:"不必拘泥于颜色，黑大褂能让每一颗致病的牙齿无处躲藏。",
        textEn:"The color is flexible. My black overcoat helps expose every single pathogenic tooth.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_12.mp3"
      }, 
      {
        textName:"嗜好",
        textZh:"绿森蚺、狼、鲨鱼……动物们的牙齿令我大开眼界，我各有珍藏；但归根结底，唯有孩童小巧的乳牙能让我心神荡漾。它们柔软、细腻，不可弥补。",
        textEn:"Green anaconda, wolf, shark ... Animal teeth really surprise me. I have collected a lot, but only the baby teeth of kids would amaze me. Those teeth are soft, delicate, and irreplaceable.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_13.mp3"
      }, 
      {
        textName:"赞赏",
        textZh:"做得很好。我会奖励你一颗太妃糖。",
        textEn:"Well done. Here is a toffee for you.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_14.mp3"
      }, 
      {
        textName:"亲昵",
        textZh:"这是你的乳牙，整整二十颗，都在这里。",
        textEn:"Your baby teeth. Twenty in total. They're all here.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_15.mp3"
      }, 
      {
        textName:"闲谈Ⅰ",
        textZh:"我与我的家庭在人类与神秘学家之间行走，我们获得钱财、地位与优渥的生活，但与此同时也失去朋友、归所与社会的认可。",
        textEn:"My family and I walked among humans and arcanists. We gain possessions, status, and decent life. But at the same time, we lose friends, belongingness, and recognition by society.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_16.mp3"
      }, 
      {
        textName:"闲谈Ⅱ",
        textZh:"我记得你，维尔汀小姐。当你还在学校时，当我还在学校时，你总是校医室的常客。一半头疼脑热是装的，另一半擦伤和摔伤是真的。我猜想你是为了安慰糖果来的，因为当你拿到太妃糖罐时，总是笑着的。",
        textEn:"I remember you, Ms. Vertin. When you were still a student, when I was the dentist there, you always visited the clinic. All those headaches you had were faked, but the wounds and bruises were real. I think your frequent visits were for the comforting candies, weren't they? Because you always smiled when you received the toffees.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_17.mp3"
      }, 
      {
        textName:"独白",
        textZh:"当你在人类之间，便以人类的方式生活。当你在神秘学家之中，又当以神秘学家的方式生活。“保持自我”总是滋养傲慢之心，紧接着带来毁灭。",
        textEn:"When in human society, do as the humans do; when in the arcanist community, do as the arcanists do. Too attached to your identity always causes arrogance and consequentially brings destruction.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_mainvoc_18.mp3"
      }, 
      {
        textName:"入队",
        textZh:"我为一颗有恙的牙而来。",
        textEn:"I'm here for a worn tooth.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_19.mp3"
      }, 
      {
        textName:"战前",
        textZh:"开始“口镜”观察。",
        textEn:"Dental checkup.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_20.mp3"
      }, 
      {
        textName:"择选咒语Ⅰ",
        textZh:"别紧张。",
        textEn:"Relax.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_21.mp3"
      }, 
      {
        textName:"择选咒语Ⅱ",
        textZh:"不必担心。",
        textEn:"No worries.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_22.mp3"
      }, 
      {
        textName:"择选高阶咒语",
        textZh:"一切都会好起来的。",
        textEn:"It will all be fine.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_23.mp3"
      }, 
      {
        textName:"择选至终的仪式",
        textZh:"我们都需要片刻安宁。",
        textEn:"We need some peaceful moments.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_24.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"请不要抵抗。",
        textEn:"Please don't resist.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_25.mp3"
      }, 
      {
        textName:"释放神秘术Ⅰ",
        textZh:"感谢配合。",
        textEn:"Thank you for your cooperation.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_26.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"呼吸。",
        textEn:"Breathe.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_27.mp3"
      }, 
      {
        textName:"释放神秘术Ⅱ",
        textZh:"我能闻到薄荷油的香气。",
        textEn:"I smell the scent of mint oil.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_28.mp3"
      }, 
      {
        textName:"召唤至终的仪式",
        textZh:"阿嚏，阿嚏，我们都跌倒。",
        textEn:"A-tishoo, a-tishoo. We all fall down.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_29.mp3"
      }, 
      {
        textName:"受敌Ⅰ",
        textZh:"嗯——",
        textEn:"Hmmm ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_30.mp3"
      }, 
      {
        textName:"受敌Ⅱ",
        textZh:"停止。",
        textEn:"Stop.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_31.mp3"
      }, 
      {
        textName:"战斗胜利",
        textZh:"它们比基金会的孩子们要顽皮得多。",
        textEn:"They are much naughtier than the Foundation's kids.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_32.mp3"
      }, 
      {
        textName:"洞悉",
        textZh:"衷心感谢，我离我的愿景又近了一步。",
        textEn:"I owe you a debt of gratitude. I am one more step closer to my dream.",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_33.mp3"
      }, 
      {
        textName:"洞悉之底",
        textZh:"睡吧，安心地睡吧，这是个甜蜜的夜晚……",
        textEn:"Sleep, sleep in peace. This is a sweet, sweet night ...",
        motion:getRandomMotion(),
        sound: mod1 + "live2d_8/305302_yaxian/sounds/En_play_hero3053_fightingvoc_34.mp3"
      }]


expList = [
  {
    expression:  "e_jingya", 
    textEn:  "表情1"
  }, 
  {
    expression:  "e_fangsong", 
    textEn:  "表情2"
  }, 
  {
    expression:  "e_renzhen", 
    textEn:  "表情3"
  }, 
  {
    expression:  "e_shengqi", 
    textEn:  "表情4"
  }, 
  {
    expression:  "e_nanguo", 
    textEn:  "表情5"
  }, 
  {
    expression:  "e_idle", 
    textEn:  "表情6"
  }, 
  {
    expression:  "e_haixiu", 
    textEn:  "表情7"
  }, 
  {
    expression:  "e_weixiao", 
    textEn:  "表情8"
  }
]
  }
}








var pio_reference
window.onload = Load_Live2d
