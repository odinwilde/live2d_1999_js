// 如果你已经有了Spine动画数据，并且这些数据已经是spine-widget.js可以理解的格式（如JSON），那么你
//可以直接使用spine-widget.js来渲染和控制动画。

// spine-skeleton-binary.js
// 主要用于将Spine导出的二进制格式转换为JSON格式。如果你已经有了正确格式的数据，你就不需要使用spine-skeleton-binary.js进行转换。

// 然而，你提到你只有.skel文件，并且你不是这个文件的开发者。在这种情况下，如果你想要将.skel文件转换
//为JSON格式，你需要使用
// spine-skeleton-binary.js
// 。这个脚本专门用于读取二进制的.skel文件并将其转换为Spine JavaScript API可以使用的JSON格式。

// 你还提到你需要的spine-skeleton-binary.js版本是Spine 3.8.95，但是你找到的只有3.6.53版本。
//spine-skeleton-binary.js文件是由Spine的创建者Esoteric Software开发和维护的。如果你需要的版本不可用，
//可能是因为Esoteric Software没有为Spine的这个特定版本发布JavaScript API。






function MySpine(t) {
    this.config = t,
    this.model = t.models[Number.parseInt(Math.random()*t.models.length)],
    this.urlPrefix = t.spineDir + this.model.name,
    this.skin = this.model.skin,
    this.skeleton = this.urlPrefix + this.model.skeleton,
    this.atlas = this.urlPrefix + this.model.atlas,
    this.widget = null,
    this.widgetContainer = document.querySelector(".myspine-spine-widget"),
    this.voiceText = document.createElement("div"),
    this.voicePlayer = new Audio,
    this.triggerEvents = ["mousedown", "touchstart", "scroll"],
    this.animationQueue = new Array,
    this.isPlayingVoice = !1,
    this.lastInteractTime = Date.now(),
    this.localX = 0,
    this.localY = 0,
    this.load()
}
MySpine.downloadBinary = function(t, e, i) {
    var n = new XMLHttpRequest;
    n.open("GET", t, !0),
    n.responseType = "arraybuffer",
    n.onload = function() {
        200 == n.status ? e(new Uint8Array(n.response)) : i(n.status, n.responseText)
    }
    ,
    n.onerror = function() {
        i(n.status, n.responseText)
    }
    ,
    n.send()
}
,
MySpine.prototype = {
    load: function() {
        let i = this.config;
        MySpine.downloadBinary(this.skeleton, t=>{
            function e(t, e) {
                for (var i in e)
                    t.style.setProperty(i, e[i])
            }
            e(this.widgetContainer, i.styles.widget),
            e(this.voiceText, i.styles.voiceText);
            t = new spine.SkeletonJsonConverter(t,1);
            t.convertToJson(),

// let skeletonJsonConverter = new spine.SkeletonJsonConverter(t, 1);
// let jsonContent = skeletonJsonConverter.convertToJson();


            new spine.SpineWidget(this.widgetContainer,{
                animation: this.getAnimationList("start")[0].name,
                skin: this.skin,
                atlas: this.atlas,
                jsonContent: t.json,
                backgroundColor: "#00000000",
                loop: !1,
                success: this.spineWidgetSuccessCallback.bind(this)
            })
        }
        , function(t, e) {
            console.error(`Couldn't download skeleton ${path}: status ${t}, ${e}.`)
        })
    },





    //These listeners wait for events like "mousedown", "touchstart", or "scroll" before 
    //starting the animations.
    spineWidgetSuccessCallback: function(t) {
        
        var e = ()=>{
            this.triggerEvents.forEach(t=>window.removeEventListener(t, e)),
            this.triggerEvents.forEach(t=>window.addEventListener(t, this.changeIdleAnimation.bind(this))),
            this.initVoiceComponents(),
            this.initWidgetActions(),
            this.initDragging(),
            this.widget.play(),
            this.playVoice(this.getVoice("start")),
            this.widgetContainer.style.display = "block"
        }
        ;




        
        //this.widgetContainer.dispatchEvent(new Event('mousedown')); // add this line

        this.widget = t,
        this.widget.pause(),
        //this.widgetContainer.style.display = "none",
        this.widgetContainer.style.display = "block",
        this.triggerEvents.forEach(t=>window.addEventListener(t, e))



        this.initWidgetActions(),

        this.widget.play()


    },
    initVoiceComponents: function() {
        this.voiceText.setAttribute("class", "myspine-voice-text"),
        this.widgetContainer.appendChild(this.voiceText),
        this.voiceText.style.opacity = 0,
        this.voicePlayer.addEventListener("timeupdate", ()=>{
            this.voiceText.scrollTo({
                left: 0,
                top: this.voiceText.offsetHeight * (this.voicePlayer.currentTime / this.voicePlayer.duration),
                behavior: "smooth"
            })
        }
        ),
        this.voicePlayer.addEventListener("ended", ()=>{
            this.voiceText.style.opacity = 0,
            this.isPlayingVoice = !1
        }
        )
    },
    initWidgetActions: function() {
        this.widget.canvas.onclick = this.interact.bind(this),
        this.widget.state.addListener({
            complete: t=>{
                (this.isPlayingVoice && t.loop) || this.isIdle() ? this.playRandAnimation({
                    name: t.animation.name,
                    loop: !0
                }) : this.playRandAnimation(this.getAnimationList("idle"))
            }
        })
    },
    initDragging: function() {


//i(t) gets the current x and y coordinates of the mouse pointe
        function i(t) {
            var e = document.documentElement.scrollLeft
              , i = document.documentElement.scrollTop;
            return t.targetTouches ? (e += t.targetTouches[0].clientX,
            i += t.targetTouches[0].clientY) : t.clientX && t.clientY && (e += t.clientX,
            i += t.clientY),
            {
                x: e,
                y: i
            }
        }
        function e(t) {
            t.cancelable && t.preventDefault()
        }


// n updates the position of the widget container
        var n = (t,e)=>{
            t = Math.max(0, t),
            e = Math.max(0, e),


           // t = Math.min(document.body.clientWidth - this.widgetContainer.clientWidth, t),
           // e = Math.min(document.body.clientHeight - this.widgetContainer.clientHeight, e),

            t = Math.min(window.innerWidth - this.widgetContainer.clientWidth, t),
            e = Math.min(window.innerHeight - this.widgetContainer.clientHeight, e),

            console.log('t:', t);
            console.log('e:', e);

            console.log('document.body.clientHeight:', document.body.clientHeight);
            console.log('this.widgetContainer.clientHeight:', this.widgetContainer.clientHeight);

            this.widgetContainer.style.left = t + "px",
            this.widgetContainer.style.top = e + "px"
        }
//o(t) is a function that calculates the initial 
//local x and y coordinates of the mouse pointer relative to the widget container.
          , o = t=>{
            var {x: e, y: t} = i(t);
            this.localX = e - this.widgetContainer.offsetLeft,
            this.localY = t - this.widgetContainer.offsetTop
        }


//The s function updates the position of the widget container as the mouse moves.
//
          , 
           s = t=>{
            var {x: e, y: t} = i(t);
           // console.log('e - this.localX:', e - this.localX);
           // console.log('t - this.localY:', t - this.localY);
            n(e - this.localX, t - this.localY),
            window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty()
        }
          , t = {
            passive: !0
        }
          , a = {
            passive: !1
        };




        //这里监听点击，然后才会在o(t)中调用e是吧。
        this.widgetContainer.addEventListener("mousedown", t=>{
            o(t),
            document.addEventListener("mousemove", s)
        }



        ),
        this.widgetContainer.addEventListener("touchstart", t=>{
            o(t),
            document.addEventListener("touchmove", e, a)
        }
        , t),
        this.widgetContainer.addEventListener("touchmove", s, t),
        document.addEventListener("mouseup", ()=>document.removeEventListener("mousemove", s)),
        this.widgetContainer.addEventListener("touchend", ()=>document.removeEventListener("touchmove", e)),
        window.addEventListener("resize", ()=>{
            let t = this.widgetContainer.style;
            var e, i;
            t.left && t.top && (e = Number.parseInt(t.left.substring(0, t.left.length - 2)),
            i = Number.parseInt(t.top.substring(0, t.top.length - 2)),
            n(e, i))
        }
        )
    },
    interact: function() {
        this.isPlayingVoice || 0 < this.animationQueue.length || !this.isIdle() ? console.warn("互动过于频繁！") : (this.lastInteractTime = Date.now(),
        this.playRandAnimation(this.getAnimationList("interact")),
        this.playVoice(this.getVoice("interact")))
    },
    getUrl: function(t) {
        // a-half-flag
        var secret = "ZmxhZ3tEb191X2g0dmVfYQ==";
        var key = "You will never find me~";
        //console.log(this.urlPrefix + t);
        return this.urlPrefix + t
    },
    getAnimationList: function(t) {
        var e = this.config.behaviors[t];
        return "start" == t? [{
            name: e.animation,
            loop: !1
        }] : e.animations.slice()
    },
    getVoice: function(t) {
        var e = this.config.behaviors[t];
        return "start" == t? {
            voice: e.voice,
            text: e.text
        } : e.voices[Math.floor(Math.random() * e.voices.length)]
    },
    playRandAnimation: function(t) {
        if (Array.isArray(t)) {
            e = t[Number.parseInt(Math.random()*t.length)];
            this.widget.state.setAnimation(0, e.name, e.loop);
        } else {
            this.widget.state.setAnimation(0, t.name, t.loop);
        }
    },
    playVoice: function(t) {
        voiceUrl = this.getUrl(t.voice);


        // 如果为空则播放五秒的空语音，仅用来显示文本框


        if (voiceUrl == this.urlPrefix) {
            console.log("空了空了")
            voiceUrl = this.config.spineDir + "default.mp3";
            
        }



        t && (this.isPlayingVoice = !0,
        this.voicePlayer.src = voiceUrl,
        this.voicePlayer.load(),
        this.voicePlayer.play().then(()=>{
            this.voiceText.innerHTML = t.text,
            this.voiceText.scrollTo(0, 0),
            this.voiceText.style.opacity = 1
        }
        , t=>{
            this.isPlayingVoice = !1,
            console.error(`无法播放音频，因为：${t}`)
        }
        ))
    },
    isIdle: function() {
        var e = this.widget.state.tracks[0].animation;
        for (const v of this.getAnimationList("idle")) {
            if (v.name == e.name) {
                return !0;
            }
        }
        return !1;
    },
    isInteract: function() {
        var e = this.widget.state.tracks[0].animation;
        for (const v of this.getAnimationList("interact")) {
            if (v.name == e.name) {
                return !0;
            }
        }
        return !1;
    },


    // This method starts the idle animations.
    changeIdleAnimation: function() {
        var t = Date.now()
          , e = t - this.lastInteractTime;
        if ((this.isIdle() && e/1e3/60 >= this.config.behaviors.idle.maxMinutes) || 
            (this.isInteract() && e/1e3 >= this.config.behaviors.interact.maxPlaySec)) {
             this.lastInteractTime = t,
             this.playRandAnimation(this.getAnimationList("idle"))
        }
    }
};