# 依赖  
[guansss/pixi-live2d-display: A PixiJS plugin to display Live2D models of any kind.](https://github.com/guansss/pixi-live2d-display)  
[Dreamer-Paul/Pio: 🎃 一个支持更换 Live2D 模型的 JS 插件](https://github.com/Dreamer-Paul/Pio)  

# 使用教程  
- 挂载都任意云端即可使用，例如：https://neocities.org/  
- 然后用<iframe src="http://localhost:8000/" style="width: 500px; height:630px"></iframe> 插入到任何支持的地方  
- 如何自己部署到html，复制Index.html的body中的部分即可。  
  
# 更新  
## Update 2024-1-13-21-37-34  
### 变：  
添加牙仙23皮肤，小鹿2皮肤，五色月2皮肤。  
随机皮肤展示  
中英双字幕，暂时无法更改设置。  
  
### 修：  
删除所有贴图白色干扰正方形  
为所有角色添加huiji.wiki提供的语言和文本，所有角色都是34个，大部分动作和语言是随机匹配。部分可能精选指定。  
模型居中修复（9的模型大部分向上移动模型高度的40%）（部分模型仍然有所偏移，比如牙仙）  
模型位置和大小优化，现在默认生成在屏幕中间，并且模型按照屏幕高度大小设置。  
手机端控制板优化，默认是悬浮后显示，手机无法显示。改成点击切换按钮显示模式。  
  
### Todo：  
单独角色模型大小可再重设。默认值不适合。  
尝试添加SPine  
以下都是api支持，但未调用。  
视角追踪  
触发区域实现  
可拖动  
  
  
## Created   
玛蒂尔达123皮肤，部分语音和文本。  
默认视线追踪无法使用，眼睛视角和鼠标有插值，不兼容模型眼睛，遂关闭。 难以修复，如果无法获得绝对准确的数值，一旦模型的scale进行设定，插值就会放大。