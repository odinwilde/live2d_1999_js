/* ----

# Pio SDK 2/3/4 support
# By: jupiterbjy
# Modify: journey-ad
# Last Update: 2021.5.4

To use this, you need to include following sources to your HTML file first.
With this script, you don't have to include `l2d.js`. Testing is done without it.
Basic usage is same with Paul-Pio.

Make sure to call `pio_refresh_style()` upon changing styles on anything related to 'pio-container' and it's children.

To change alignment, modify variable `pio_alignment` to either `left` or `right`, then call `pio_refresh_style()`.

<script src="https://cubism.live2d.com/sdk-web/cubismcore/live2dcubismcore.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/dylanNew/live2d/webgl/Live2D/lib/live2d.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/pixi.js@5.3.6/dist/pixi.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/pixi-live2d-display/dist/index.min.js"></script>

If you have trouble setting up this, check following example's sources.
https://jupiterbjy.github.io/PaulPio_PIXI_Demo/

---- */
//加载模型？被pio.js调用
// 这里加载模型，有一个回调函数。
//通过load.js里的model获得地址
//在pio.js里获得元素
function loadlive2d(canvas_id, json_object_or_url, on_load) {
    // Replaces original l2d method 'loadlive2d' for Pio.
    // Heavily relies on pixi_live2d_display.

    
    console.log("[Pio] Loading new model")

    const canvas = document.getElementById(canvas_id)

    // When pio was start minimized on browser refresh or reload,
    // canvas is set to 0, 0 dimension and need to be changed.
    if (canvas.width === 0) {
        canvas.removeAttribute("height")
        pio_refresh_style()
    }

    // Try to remove previous model, if any exists.
    try {
        app.stage.removeChildAt(0)
    } catch (error) {

    }

    let model = PIXI.live2d.Live2DModel.fromSync(json_object_or_url, { autoInteract: false })




    model.once("load", () => {
        app.stage.addChild(model);

        console.log("我又加载啦")

        //draggable(model);
        //addFrame(model);
        //addHitAreaFrames(model);


        //元素的高度除以模型的高度？ 
        //canvas现在是浏览器高度/
        const vertical_factor = canvas.height / model.height
        console.log("canvas.height"+canvas.height+"  canvas.weight"+canvas.weight+"  model.height"+model.height+"  .model.weight"+model.weight + "  vertical_factor" + vertical_factor)
// canvas.height600  canvas.weightundefined  model.height5318  .model.weightundefined  vertical_factor0.11282437006393381

        //将模型设置为0.1128,等于是模型的长和宽乘0.1128.
        //得到的结果就是canvas.heith
        //此时canvas的宽才刚刚得出，也就是模型的宽=396
        //model.scale.set(vertical_factor)
        //console.log("canvas.height"+canvas.height+"  canvas.weight"+canvas.weight+"  model.height"+model.height+"  .model.weight"+model.weight + "  vertical_factor" + vertical_factor)
//canvas.height600  canvas.weightundefined  model.height600  .model.weightundefined  vertical_factor0.11282437006393381




        //模型定位算法。
        //
        // if(model.internalModel.settings.name === "305602_jiexika"){
        //     model.scale.set(vertical_factor*1.4)
        //     model.y = 270 //正值是向下移动
        //     model.x = -90 //正值是向右移动
        //     //canvas.width = 396
        //     //canvas.height = model.height 
        // }else if (model.internalModel.settings.name === "303802_wuseyue"){
        //     model.scale.set(vertical_factor*1.3)
        //     model.y = 280
        //     model.x = -120
        //     //canvas.width = 396ss
        //     //canvas.height = model.height 
        // }else if (model.internalModel.settings.name === "305302_yaxian" || model.internalModel.settings.name === "v1a5_305303_yaxian" ){
        //     model.scale.set(vertical_factor*1.2)
        //     model.y = 260
        //     model.x = -99
        //     //canvas.width = 396
        //     //canvas.height = 800


        // }else{
        //     model.scale.set(vertical_factor*1)
        //     model.y = 270
        //     model.x = -40
        //     //canvas.width = 396
        //     //canvas.height = model.height 
        // }

        console.log("canvas.height"+canvas.height+"  canvas.weight"+canvas.weight+"  model.height"+model.height+"  .model.weight"+model.weight + "  vertical_factor" + vertical_factor)

        // // match canvas to model width
        // canvas.width = 396
        // canvas.height = 600

        //match canvas to model width



        //如何让模型正好完美嵌入整个页面？
        //也就是模型区域的高度=页面的高度。
        //
        const windows_factor = innerHeight / model.height

        
        model.scale.set(windows_factor)



//重置，来探讨一下，模型居中算法.
        //算一下，模型高出区域的大小
        // 人物长度397
        // 显示长度237
        // 区域长度437.6
        // (437.6-397)/2=20.3 人物居中，上下间距
        // 437.6-237-20.3-20.3=160 上身长
        // 160-20.3=3
        // 397-237=160 上身=160
        // 显示的长度人物的60%。
        // 模型整体向下移动多少，人物处于居中？
        // 向下一个上身，再移动一个间距。
        // 160+20.3=180.3
        // 180.3/437.6=0.412
        // 大概是40%。
        // 向下移动模型高度的40%即可让模型归位置，而不是居中。 所以这是最终的一个修正值。 model.height * 0.4

        //模型的左上角是0原点。
        //模型居中Y点，在windosheight的上面模型，多模型一半的高度。
        //(innerHeight / 2) + (model.height/2)
        //位置修正： (innerHeight / 2) - (model.height/2) + (model.height * 0.4)

        


        //var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        //console.log("窗口宽度：" + windowWidth);
        //var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        //model.x = canvas.width/2
        model.x = 0
        model.x = (innerWidth / 2) - (model.width/2);

        //model.y = (innerHeight / 2)
        model.y = (innerHeight / 2) - (model.height/2) + (model.height * 0.4)





        pio_refresh_style()

        // check alignment, and align model to corner 这是干啥的？
        // if (document.getElementsByClassName("pio-container").item(0).className.includes("left")){
        //     model.x = 0
        // } else {
        //     model.x = canvas.width - model.width
        // }

        // Hit callback definition
        model.on("hit", hitAreas => {
            if (hitAreas.includes("body")) {
                console.log("[Pio] Touch on body (SDK2)")
                model.motion('tap_body')

            } else if (hitAreas.includes("Body")) {
                console.log("[Pio] Touch on body (SDK3/4)")
                model.motion("Tap")

            } else if (hitAreas.includes("head") || hitAreas.includes("Head")){
                console.log("[Pio] Touch on head")
                model.expression()
            }
        })
        
        on_load(model)
    })

    return model
}


function _pio_initialize_container(){

    // Generate structure
    let pio_container = document.createElement("div")


    pio_container.classList.add("pio-container")
    pio_container.id = "pio-container"



    document.body.insertAdjacentElement("beforeend", pio_container)

    // Generate action
    let pio_action = document.createElement("div")
    pio_action.classList.add("pio-action")
    pio_container.insertAdjacentElement("beforeend", pio_action)

    // Generate canvas
    let pio_canvas = document.createElement("canvas")
    pio_canvas.id = "pio"
    pio_container.insertAdjacentElement("beforeend", pio_canvas)

    console.log("[Pio] Initialized container.")
}





function pio_refresh_style(){
    // Always make sure to call this after container/canvas style changes!
    // You can set alignment here, but still you can change it manually.

    let pio_container = document.getElementsByClassName("pio-container").item(0)



    //pio_container.classList.remove("left", "right") - 从 pio_container 元素的类名中移除 "left" 和 "right" 这两个
    //类名。这意味着在执行这行代码后，pio_container 不再具有 "left" 和 "right" 类。

    //pio-action 的位置为什么由这个控制。
    let pio_alignment = "right";
    pio_container.classList.remove("left", "right")
    pio_container.classList.add(pio_alignment)
    //css 中有left和right的class。这里添加谁就是谁了。
    //之前的代码left里是right，right里是left。搞不懂。


    // app.resizeTo = document.getElementById("pio")
}
// change alignment to left by modifying this value in other script.
// Make sure to call `pio_refresh_style` to apply changes!

function _pio_initialize_pixi() {
    // Initialize html elements and pixi app.
    // Must run before pio init.

    _pio_initialize_container()

    app = new PIXI.Application({
        view: document.getElementById("pio"),
        transparent: true,
        resizeTo: window,
        //backgroundColor: 0x3333333,

        //: document.getElementById("pio"),
        // resizeTo: {
        //     width: 900,
        //     height: 6900
        // },
        
        autoStart: true,
    })

    pio_refresh_style()
}





let app
window.addEventListener("DOMContentLoaded", _pio_initialize_pixi)



function draggable(model) {
    model.buttonMode = true;
    model.on("pointerdown", (e) => {
      model.dragging = true;
      model._pointerX = e.data.global.x - model.x;
      model._pointerY = e.data.global.y - model.y;
    });
    model.on("pointermove", (e) => {
      if (model.dragging) {
        model.position.x = e.data.global.x - model._pointerX;
        model.position.y = e.data.global.y - model._pointerY;
      }
    });
    model.on("pointerupoutside", () => (model.dragging = false));
    model.on("pointerup", () => (model.dragging = false));
  }
  
  function addFrame(model) {
    const foreground = PIXI.Sprite.from(PIXI.Texture.WHITE);
    foreground.width = model.internalModel.width;
    foreground.height = model.internalModel.height;
    foreground.alpha = 0.2;
  
    model.addChild(foreground);
  
    checkbox("Model Frames", (checked) => (foreground.visible = checked));
  }
  
  function addHitAreaFrames(model) {
    const hitAreaFrames = new live2d.HitAreaFrames();
  
    model.addChild(hitAreaFrames);
  
    checkbox("Hit Area Frames", (checked) => (hitAreaFrames.visible = checked));
  }
  
  function checkbox(name, onChange) {
    const id = name.replace(/\W/g, "").toLowerCase();
  
    let checkbox = document.getElementById(id);
  
    if (!checkbox) {
      const p = document.createElement("p");
      p.innerHTML = `<input type="checkbox" id="${id}"> <label for="${id}">${name}</label>`;
  
      document.getElementById("control").appendChild(p);
      checkbox = p.firstChild;
    }
  
    checkbox.addEventListener("change", () => {
      onChange(checkbox.checked);
    });
  
    onChange(checkbox.checked);
  }