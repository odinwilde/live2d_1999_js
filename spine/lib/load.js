


let myspine = {
    enable: true,
    spineDir: "/spine/",
    models: [
      {
        name: "sekai_atlas/",
        skin: "default",
        atlas: "sekai_atlas.atlas",
        skeleton: "sekai_atlas.skel"
      },
      {
        name: "dyn_illust_char_1012_skadi2/",
        skin: "model_example.skin",
        atlas: "dyn_illust_char_1012_skadi2.atlas",
        skeleton: "dyn_illust_char_1012_skadi2.skel"
      }
    ],
    styles: {
      widget: {
        width: "200px",
        height: "200px"
      },
      voiceText: {
        color: "#e6e6e6"
      }
    },
    behaviors: {
      start: {
        animation: "w_emu_run01_f",
        voice: "",
        text: "Ohhhhh好耶"
      },
      idle: {
        maxMinutes: 1,
        animations: [
          {
            name: "m_cool_idle01_f",
            loop: false
          },
          {
            name: "m_cool_joy01_f",
            loop: false
          }
        ],
        voices: [
          {
            voice: "",
            text: "不买立省百分百"
          }
        ]
      },
      interact: {
        maxPlaySec: 3,
        animations: [
          {
            name: "w_normal_joy01_b",
            loop: false
          },
          {
            name: "m_cool_angry01_f",
            loop: false
          }
        ],
        voices: [
          {
            voice: "",
            text: "既见未来，为何不buy"
          }
        ]
      }
    }
  };
    
// const {
//     enable,
//     spineDir,
//     models,
//     styles,
//     behaviors
// } = myspine;





new MySpine({
    spineDir: myspine.spineDir,
    models: myspine.models,
    styles: myspine.styles,
    behaviors: myspine.behaviors,
    
});

console.log("spineDir:", myspine.spineDir);
console.log("models:", myspine.models);
console.log("styles:", myspine.styles);
console.log("behaviors:", myspine.behaviors);
